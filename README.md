# 💤 NeoVim IDE

## Requirements

### Arch Linux

1. System `yay -S gcc wget npm unzip --noconfirm`
2. Languages `yay -S python python-pip gopls delve --noconfirm`
3. Neovim > 0.10 `yay -S neovim --noconfirm`
4. LazyVim `yay -S fd ripgrep lazygit lazydocker luarocks prettier prettierd markdownlint-cli nodejs-markdown-toc markdown-toc-cli nodejs-neovim tree-sitter-cli stylua fish shfmt python-pynvim-git --noconfirm`
5. Cloning `git clone https://gitlab.com/zafhiel/nvim ~/.config/nvim`
6. run command `MasonUpdate` and `LazyHealth` you can find in `<leader>sC`
7. Optional for Latex (no tested yet) `sudo pacman -Syu texlive-core texlive-fontsextra texlive-latexextra texlive-science biber texlive-bibtexextra texlive-latexextra texlive-pictures texlive-binextra texlive-xetex texlive-langgreek --noconfirm`

## Fonts

- Install Hack Nerd Font `yay -S ttf-hack-nerd`

## lazygit

1. Install `yay -S git-delta --noconfirm`
2. Modified the file `~/.config/lazygit/config.yml`

```yml
git:
  paging:
    colorArg: always
    pager: delta --dark --line-numbers --paging=never --file-style='"#dbbc7f" "#45443c"'  --file-decoration-style='"#dbbc7f" "#45443c" ul ol' --hunk-header-style="omit-code-fragment" --line-numbers-zero-style='"#9da9a0" "#2e383c"' --line-numbers-plus-style='"#a7c080" "#3c4841"' --line-numbers-minus-style='"#e67e80" "#4c3743"' --line-numbers-right-style='"#d3c6aa"' --line-numbers-left-style='"#d3c6aa"' --side-by-side --syntax-theme=ansi
# Style for code fragment, if its active
# --hunk-header-decoration-style='"#7fbbb3" "#384b55" box' --hunk-header-line-number-style='"#7fbbb3"' --hunk-header-style="omit-code-fragment"
# How disabled code fragment
# --hunk-header-style="omit-code-fragment"
```
