return {

  -- everforest hard colors
  -- bg_dim = "#1E2326"
  -- bg0 = "#272E33"
  -- bg1 = "#2E383C"
  -- bg2 = "#374145"
  -- bg3 = "#414B50"
  -- bg4 = "#495156"
  -- bg5 = "#4F5B58"
  -- bg_red = "#4C3743"
  -- bg_visual = "#493B40"
  -- bg_yellow = "#45443c"
  -- bg_green = "#3C4841"
  -- bg_blue = "#384B55"
  -- red = "#E67E80"
  -- orange = "#E69875"
  -- yellow = "#DBBC7F"
  -- green = "#A7C080"
  -- blue = "#7FBBB3"
  -- aqua = "#83C092"
  -- purple = "#D699B6"
  -- fg = "#D3C6AA"
  -- statusline1 = "#A7C080"
  -- statusline2 = "#D3C6AA"
  -- statusline3 = "#E67E80"
  -- grey0 = "#7A8478"
  -- grey1 = "#859289"
  -- grey2 = "#9DA9A0"

  {
    "catppuccin/nvim",
    lazy = true,
    name = "catppuccin",
    priority = 1000,
    opts = {
      flavour = "latte",
      color_overrides = {
        latte = {
          -- custom everforest dark hard port
          rosewater = "#fed1cb",
          flamingo = "#ff9185",
          pink = "#d699b6",
          mauve = "#cb7ec8",
          red = "#E67E80",
          maroon = "#e67e80",
          peach = "#e69875",
          yellow = "#d3ad63",
          green = "#83C092",
          teal = "#6db57f",
          sky = "#7fbbb3",
          sapphire = "#60aaa0",
          blue = "#A7C080", -- green
          lavender = "#e0d3d4",
          base = "#272E33", -- bg0
          crust = "#1E2326", -- bg_dim
          mantle = "#2E383C", -- bg1

          surface0 = "#374145", -- bg2
          surface1 = "#414B50", -- bg3
          surface2 = "#495156", -- bg4

          overlay0 = "#7A8478", -- grey0
          overlay1 = "#859289", -- grey1
          overlay2 = "#9DA9A0", -- grey2

          text = "#D3C6AA", --fg
          subtext0 = "#D3C6AA", -- statusline2
          subtext1 = "#9DA9A0", -- gray2
        },
      },
      integrations = {
        aerial = true,
        alpha = true,
        cmp = true,
        dashboard = true,
        flash = true,
        gitsigns = true,
        headlines = true,
        illuminate = true,
        indent_blankline = { enabled = true },
        leap = true,
        lsp_trouble = true,
        mason = true,
        markdown = true,
        mini = true,
        native_lsp = {
          enabled = true,
          underlines = {
            errors = { "undercurl" },
            hints = { "undercurl" },
            warnings = { "undercurl" },
            information = { "undercurl" },
          },
        },
        navic = { enabled = true, custom_bg = "lualine" },
        neotest = true,
        neotree = true,
        noice = true,
        notify = true,
        semantic_tokens = true,
        telescope = true,
        treesitter = true,
        treesitter_context = true,
        which_key = true,
      },
    },
  },
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "catppuccin",
    },
  },
}
