return {
  { "ellisonleao/gruvbox.nvim" },
  { "sainnhe/edge" },
  { "sainnhe/gruvbox-material" },
  { "rose-pine/neovim", name = "rose-pine" },
  {
    "sainnhe/everforest",
    config = function()
      vim.g.everforest_enable_italic = 1
      vim.g.everforest_background = "hard"
    end,
  },
}
