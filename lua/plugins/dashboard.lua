return {
  "nvimdev/dashboard-nvim",
  lazy = false,
  opts = function()
    local opts = {
      theme = "hyper",
      hide = {
        statusline = false,
      },
      config = {
        week_header = {
          enable = true,
        },
        shortcut = {
          {
            icon_hl = "@variable",
            desc = " Files",
            group = "DiagnosticHint",
            action = LazyVim.pick(),
            key = "f",
          },
          { action = LazyVim.pick("live_grep"), group = "Number", desc = "  Find Text", key = "g" },
          { desc = "󰊳 Update", group = "@property", action = "Lazy update", key = "u" },
          { action = LazyVim.pick.config_files(), desc = " Config", key = "c" },
        },
      },
    }

    if vim.o.filetype == "lazy" then
      vim.cmd.close()
      vim.api.nvim_create_autocmd("User", {
        pattern = "DashboardLoaded",
        callback = function()
          require("lazy").show()
        end,
      })
    end

    return opts
  end,
}
