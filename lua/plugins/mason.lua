return {
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "go-debug-adapter",
        "gofumpt",
        "goimports",
        "goimports-reviser",
        "golangci-lint",
        "golangci-lint-langserver",
        "golines",
        "gomodifytags",
        "google-java-format",
        "gopls",
        "gospel",
        "gotests",
        "gotestsum",
        "json-to-struct",
        "gci",
        "crlfmt",
        "nilaway",
        "impl",
        "iferr",
        "ltex-ls",
        "debugpy",
        "black",
        "prettier",
      },
    },
  },
}
